import asyncio
from typing import Union

import aiodocker
from aiodocker import Docker
from aiodocker.containers import DockerContainer
from aiodocker.images import DockerImages

from aiohttp_socks import SocksConnector

from common import Singleton


class TorConnector(metaclass=Singleton):
    _config = {
        'Image': 'peterdavehello/tor-socks-proxy',
        'HostConfig': {
            'PortBindings': {
                '9150/tcp': [{'HostPort': '9150'}],
            }
        },
    }

    def __init__(self):
        self._lock = asyncio.Lock()
        self._docker: Docker = aiodocker.Docker()
        self._container: Union[DockerContainer, None] = None
        self._restarted_in_session = False

    def get_connector(self) -> SocksConnector:
        return SocksConnector.from_url('socks5://127.0.0.1:9150')

    async def start(self):
        if self._container is None:
            await aiodocker.images.DockerImages(self._docker).pull(
                self._config['Image']
            )
            self._container = await self._docker.containers.create_or_replace(
                config=self._config,
                name='tor'
            )
            await self._container.start()

    async def restart(self):
        if not self._lock.locked():
            raise RuntimeError('Connector must be blocked')

        await self._container.restart()

    async def __aenter__(self):
        await self._lock.acquire()
        if self._container is None:
            raise RuntimeError('Tor container not started')
        return self

    async def __aexit__(self, exc_type, exc, tb):
        self._lock.release()

import asyncio
from typing import Dict

import aiohttp

from proxy import TorConnector


class ApiError(Exception):
    ...


async def get_bank_info(bin_number: int) -> Dict:
    async def make_request():
        url = f'https://lookup.binlist.net/{bin_number}'
        async with TorConnector() as connector:
            async with aiohttp.ClientSession(
                    connector=connector.get_connector()
            ) as session:
                async with session.get(url) as resp:
                    if resp.status == 429:
                        await connector.restart()
                        return await get_bank_info(bin_number)
                    if resp.status > 200:
                        text = await resp.text()
                        raise ApiError(f'msg: {text}, code: {resp.status}')

                    return await resp.json()

    return await asyncio.wait_for(make_request(), timeout=60)

from collections import deque

from aiohttp import web
from bin_api import get_bank_info
from proxy import TorConnector


def json_error(status_code: int, exception: Exception) -> web.Response:
    return web.json_response(
        status=status_code,
        data={
            'error': exception.__class__.__name__,
            'detail': str(exception)
        }
    )


async def error_middleware(app: web.Application, handler):
    async def middleware_handler(request):
        try:
            response = await handler(request)
            if response.status == 404:
                return json_error(response.status, Exception(response.message))
            return response
        except web.HTTPException as ex:
            if ex.status == 404:
                return json_error(ex.status, ex)
            raise
        except Exception as e:
            return json_error(500, e)

    return middleware_handler


async def bank_info(request: web.Request) -> web.Response:
    bin_number = request.match_info['bin_number']
    info = await get_bank_info(bin_number)
    request.app['data'].append(info)
    return web.json_response(info)


async def history(request: web.Request) -> web.Response:
    return web.json_response(list(request.app['data']))


def main():
    async def start_proxy(app: web.Application):
        await TorConnector().start()

    async def init_values(app: web.Application):
        app['data'] = deque(maxlen=10)

    app = web.Application(middlewares=[error_middleware])
    app.add_routes([web.get(r'/{bin_number:\d+}', bank_info)])
    app.add_routes([web.get('/history', history)])
    app.on_startup.append(start_proxy)
    app.on_startup.append(init_values)
    web.run_app(app)


if __name__ == '__main__':
    main()
